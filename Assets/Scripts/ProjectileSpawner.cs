﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{
    public Transform projectilePrefab, projectileSpawnPoint;
    public int initialProjectilePoolSize = 10;
    List<Transform> projectiles;
    public float projectileSpawnDelay = 0.15f;
    bool playerShooting = false;
    

    void Start()
    {
        //fill the projectile pool with projectile prefabs
        projectiles = new List<Transform>();

        for (int i = 0; i < initialProjectilePoolSize; i++)
        {
            Transform obj = Instantiate(projectilePrefab);
            obj.gameObject.SetActive(false);
            projectiles.Add(obj);
        }
        
    }

    /// <summary>
    /// Set the player character's shooting state: "true" for auto fire
    /// </summary>
    /// <param name="canShoot"></param>
    public void UpdateShootingState(bool canShoot)
    {
        if(playerShooting == canShoot)
        {
            return;
        }

        playerShooting = canShoot;

        if(playerShooting == true)
        {
            StartCoroutine(PlayerShooting());
        }
    }

    IEnumerator PlayerShooting()
    {
        while (playerShooting == true)
        {
            Fire();
            yield return new WaitForSeconds(projectileSpawnDelay);
        }
    }

    void Fire()
    {
        for (int i = 0; i < projectiles.Count; i++)
        {
            if(!projectiles[i].gameObject.activeInHierarchy)
            {
                projectiles[i].position = projectileSpawnPoint.position;
                projectiles[i].rotation = projectileSpawnPoint.rotation;
                projectiles[i].gameObject.SetActive(true);
                return;
            }
        }
        //if no available projectile in pool - add a new projectile
        Transform obj = Instantiate(projectilePrefab);
        obj.gameObject.SetActive(false);
        obj.position = projectileSpawnPoint.position;
        obj.rotation = projectileSpawnPoint.rotation;
        projectiles.Add(obj);
        obj.gameObject.SetActive(true);
        
    }
}
