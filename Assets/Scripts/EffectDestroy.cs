﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectDestroy : MonoBehaviour
{
    float lifeTime = 1.0f;

    private void OnEnable()
    {
        Destroy(gameObject, lifeTime);
    }
}
