﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Locked to X Y movement as per brief - no Y movement or rotation.
/// </summary>
public class CameraFollow : MonoBehaviour
{
    Transform player, camTransform;
    public Vector3 cameraOffset;
    public float followSpeed = 8.0f;

    void Start()
    {
        //cache player and camera Transform, and set cameraOffset to initial position.
        player = PlayerController.Controller.gameObject.transform;
        camTransform = this.transform;
        cameraOffset = player.position - camTransform.position; //cache camera offset based on Camera position at level start
    }
    
    void LateUpdate()
    {
        Vector3 desiredPos = player.position - cameraOffset;

        camTransform.position = Vector3.Lerp(camTransform.position, desiredPos, followSpeed * Time.deltaTime);
    }
}
