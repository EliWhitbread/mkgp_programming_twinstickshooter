﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Image))]
public class VirtualJoystick : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    //[SerializeField]RectTransform ;
    [SerializeField]RectTransform joystickBackPlane, draggableJoysticIcon;
    Vector3 joysticRestingPosition;

    //level of control for the joystick
    public bool isolateLookAndShoot = false;


    void Start()
    {
        joystickBackPlane = GetComponent<Image>().rectTransform;
        if(joystickBackPlane == null)
        {
            Debug.Log("No Joystick Plane found!");
        }
        draggableJoysticIcon = transform.GetChild(0).GetComponent<Image>().rectTransform;
        if (draggableJoysticIcon == null)
        {
            Debug.Log("No Joystic Icon found!");
        }
        
        joysticRestingPosition = draggableJoysticIcon.position;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        SetDraggableIconPosition(eventData);
        if(isolateLookAndShoot == true)
        {
            PlayerController.Controller.PlayerShooting = true;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (draggableJoysticIcon == null)
        {
            return;
        }

        SetDraggableIconPosition(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if(draggableJoysticIcon == null)
        {
            return;
        }

        //reset to resting position
        draggableJoysticIcon.position = joysticRestingPosition;

        //limit undesired player movement
        if(isolateLookAndShoot == false)
        {
            PlayerController.Controller.MoveDirection = Vector3.zero;
            PlayerController.Controller.SetAnimatorSpeed(0.0f);
        }
        else
        {
            PlayerController.Controller.LookDirection = Vector3.zero;
            PlayerController.Controller.PlayerShooting = false;
        }
    }

    void SetDraggableIconPosition(PointerEventData data)
    {
        Vector2 touchPosition;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(joystickBackPlane, data.position, data.pressEventCamera, out touchPosition))
        {
            //convert touchPosition to a ratio of 0 - 1 bassed on the background plane rectTransform
            touchPosition.x = (touchPosition.x / joystickBackPlane.sizeDelta.x);
            touchPosition.y = (touchPosition.y / joystickBackPlane.sizeDelta.y);

            //compensate for joystickBackPlane (parent) pivot point 
            float _x = (joystickBackPlane.pivot.x == 1) ? touchPosition.x * 2 + 1 : touchPosition.x * 2 - 1;
            float _y = (joystickBackPlane.pivot.y == 1) ? touchPosition.y * 2 + 1 : touchPosition.y * 2 - 1;

            Vector3 joyPos = new Vector3(_x, 0.0f, _y);

            //normalize the position data if player's input extends past the joystick background bounds - also clamps draggableJoysticIcon's position to the same.
            joyPos = (joyPos.sqrMagnitude > 1.0f * 1.0f) ? joyPos.normalized : joyPos;

            draggableJoysticIcon.anchoredPosition = new Vector3(joyPos.x * (joystickBackPlane.sizeDelta.x / 2), joyPos.z * (joystickBackPlane.sizeDelta.y / 2));
            
            if(isolateLookAndShoot == false)
            {
                PlayerController.Controller.MoveDirection = joyPos;
            }
            else
            {
                PlayerController.Controller.LookDirection = joyPos;
            }
            
        }
    }

   
}
