﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Controller { get; private set; }

    Transform playerTransform;
    public float moveSpeed = 5.0f;
    Vector3 moveDirection, lookDirection;
    bool playerShooting = false;
    ProjectileSpawner projectileSpawner;
    Animator anim;


    public Vector3 MoveDirection
    {
        set { moveDirection = value; }
    }

    public Vector3 LookDirection
    {
        set { lookDirection = value; }
    }

    public bool PlayerShooting
    {
        set { playerShooting = value; SetPlayerShootingState(playerShooting); }
    }

    private void Awake()
    {
        if(Controller != null && Controller != this)
        {
            Destroy(this);
        }
        else
        {
            Controller = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        playerTransform = this.transform; //cache the Player GameObject Transform
        projectileSpawner = gameObject.GetComponent<ProjectileSpawner>(); //cache the projectile spawner
        projectileSpawner.UpdateShootingState(false);
        anim = gameObject.GetComponent<Animator>(); //cache the Animator
        SetPlayerShootingState(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(playerShooting == false)
        {
            if(moveDirection == Vector3.zero)
            {
                return;
            }
            PlayerLook(moveDirection);
            PlayerMove(moveDirection);
        }
        else
        {
            if(moveDirection != Vector3.zero)
            {
                PlayerMove(moveDirection);
            }
            if(lookDirection != Vector3.zero)
            {
                PlayerLook(lookDirection);
            }
        }
    }

    /// <summary>
    /// move the player
    /// </summary>
    /// <param name="dir">move direction</param>
    void PlayerMove(Vector3 dir)
    {
        Vector3 moveDir = new Vector3(dir.x, 0.0f, dir.z);
        playerTransform.Translate(moveDir * (moveSpeed * Time.deltaTime), Space.World);

        //get movement speed based on directional input. TODO - good for prototype only - change to factor in player transform.forward if more animations are added.
        float moveX = Mathf.Abs(dir.x);
        float moveZ = Mathf.Abs(dir.z);
        float animSpeed = (moveX + moveZ) / 2;
        
        SetAnimatorSpeed(animSpeed);
    }

    /// <summary>
    /// Rotate the player to face to desired direction
    /// </summary>
    /// <param name="dir">target direction</param>
    void PlayerLook(Vector3 dir)
    {
        playerTransform.eulerAngles = new Vector3(0.0f, Mathf.Atan2(dir.x, dir.z) * 180 / Mathf.PI, 0.0f);
    }

    public void SetAnimatorSpeed(float speed)
    {
        anim.SetFloat("MoveSpeed", speed);
    }

    void SetPlayerShootingState(bool canShoot)
    {
        projectileSpawner.UpdateShootingState(canShoot);

        anim.SetBool("Shoot", canShoot);
    }
}
