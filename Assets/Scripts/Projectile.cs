﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float moveSpeed = 50.0f, lifeTime = 2.0f;
    public int hitValue = 80;
    Transform projectileTransform;

    private void OnEnable()
    {
        if(projectileTransform == null)
        {
            projectileTransform = this.transform;
        }

        Invoke("DisableProjectile", lifeTime);
    }

    void Update()
    {
        projectileTransform.Translate(Vector3.forward * (moveSpeed * Time.deltaTime));
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy _en = other.GetComponent<Enemy>();
        if(_en != null)
        {
            _en.TakeDamage(hitValue);
        }

        CancelInvoke();
        DisableProjectile();
    }

    //Disable gameObject - ready to be reused by projectile pool
    void DisableProjectile()
    {
        gameObject.SetActive(false);
    }
}
