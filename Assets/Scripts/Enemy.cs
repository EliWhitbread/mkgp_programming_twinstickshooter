﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health = 100;

    public void TakeDamage(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            GameObject effect = Instantiate(Resources.Load("ExplosionEffect", typeof(GameObject))) as GameObject; //instantiate explosion particle effect on Enemy death.
            effect.transform.position = gameObject.transform.position;
            gameObject.SetActive(false);
        }
    }
}
